import natural from "natural";
import {removeStopwords, porBr} from "stopword";
import {readFileSync} from 'fs';
//recebe o corpo de uma notícia e retorna um array de palavras, filtradas por stopwords
//
const CustomWords = () => {
    const stopwords = readFileSync('stopwords.txt', 'utf8');
    const tokenizer = new natural.AggressiveTokenizerPt()
    return tokenizer.tokenize(stopwords);
}
const Tokenize = (news) => {
    const customStopWords = CustomWords();
    const tokenizer = new natural.AggressiveTokenizerPt()
    const output = tokenizer.tokenize(news)
    const custom = removeStopwords(output, [...porBr, ...customStopWords]);
    return custom.map((word) => {
        return word.toLowerCase();
    });
}
export default Tokenize;
