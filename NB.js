import CSVData from './modules/CSVData.js'
import trainTestSplit from 'train-test-split';
import Train from './modules/Train.js';
import Test from './modules/Test.js'

//parametros do teste. Modifique aqui essas variaveis pra testar diferentes configurações de resultado
// split = quantos porcento do dataset vai ser destinado a treino. ex: 0.8 = 80% treino, 20% teste
// seed = numero especifico que cria um resultado deterministico por separar treino e teste da mesma maneira sempre que rodado
// alphapad = correção do dataset, precisa ser pelo menos 1  https://en.wikipedia.org/wiki/Additive_smoothing
const NB = () => {
    const split = 0.7;
    const seed = Math.random();
    const alphaPad = 2;


    const csv = CSVData();
    //input = test.corpo after map
    //divide o dataset em treino e teste: 70% treino, seed aleatorio
    const [train, test] = trainTestSplit(csv, split, seed);
    const quantidade = train.length;
    const verdadeiras = train.filter(entry => entry.tipo == 1);
    const falsas = train.filter(entry => entry.tipo == 0);
    //probabilidade base de noticia verdadeira (45.77%)
    const baseVerdadeira = verdadeiras.length / quantidade;
    //probabilidade base de noticia falsa (54.22%)
    const baseFalsa = falsas.length / quantidade;
    const trained = Train(verdadeiras, falsas);

    const model = {
        'probV': baseVerdadeira,
        'probF': baseFalsa,
        'classV': trained.classV,
        'classF': trained.classF,
        'alpha': alphaPad / (trained.vocab),
        'conMatrix': {
            'VP': 0,
            'VN': 0,
            'FP': 0,
            'FN': 0,
        }
    }

    const final = Test(model, test);

    const conMatrix = final.conMatrix;
    console.log(conMatrix);
    const fCertos = ((conMatrix.VN / (conMatrix.VN + conMatrix.FN)) * 100).toFixed(2);
    const sens = ((conMatrix.VN / (conMatrix.VN + conMatrix.FP))*100).toFixed(2);
    const f1 = ((2*conMatrix.VP / (2*conMatrix.VP + conMatrix.FP + conMatrix.FP))* 100).toFixed(2);
    const total = conMatrix.VP + conMatrix.VN + conMatrix.FP + conMatrix.FN;
    const totalCertos = (((conMatrix.VP + conMatrix.VN) / total) * 100).toFixed(2);

    console.log('Acurácia geral: ' + totalCertos + '%');
    console.log('Precisão fake: ' + fCertos + '%');
    console.log('Sensibilidade: ' +  sens+ '%');
    console.log('f1 score: ' +  f1+ '%');
    return model;
}
NB();
export default NB;
