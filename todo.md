# Passo a passo
## Início
- [x] Importar banco de dados
- [x] Pré processamento de texto
- [x] Separar as entradas em classes
- [x] Calcular a probabilidade base das classes
- [x] Dividir o dataset em treino e teste

## Algoritmo de classificação : Treino
### Fórmula base: quantas vezes a palavra apareceu/ quantidade de palavras (Por classe)
>entrada: uma noticia depois do pré processamento

>classificador: Uma coleção (array) de palavras. Existe um para cada tipo de noticia, verdadeiro e falsa
- Para cada entrada:
    - Verificar se entrada é verdadeira ou falsa

### Para Cada palavra na entrada
- verificar se a palavra existe no classificador do tipo correspondente (verdadeiro ou falso)
- se não existe, adicionar com contador=1.
- Seexiste, aumentar o contador em 1

### Depois de processar todas as palavras
>Vocabulario : Contagem de palavras únicas de um classificador
- [x] Para cada palavra nos classificadores, adicionar probabilidade (contador/vocabulario)
- [x] Armazenar contagem de palavras total de cada classificador como **vocabulario** para calculagem do mecanismo de correção
## Teste
>baseAlpha: mecanismo de correção para palavras que não existem em um classificador

**baseAlpha = 1/vocabulario** . Assume que a palavra ocorreu pelo menos uma vez

Caso isso nao seja levado em consideração, palavras que nao ocorreram em um set automaticamente invalidam entradas que a contenham. (Multiplicação por 0)

### O teste deve ser executo pelos dois classificadores
- Para cada palavra na entrada teste, encontrar a palavra no classificador correspondente
- Caso não encontre assumir probabilidade = baseAlpha
- Caso encontre: probabilidade = (probabilidade + baseAlpha)
- Multiplicar as probabilidaes Base * chance de cada palavra
- Comparar qual dos dois classificadores tem uma probabilidade maior
- A entrada pertence ao grupo com a maior probabilidade
# Estatisticas
- Observar o campo tipo da entrada de teste
- Validar se o algoritmo acertou ou não

# Encerramento
- Imprimir informações
