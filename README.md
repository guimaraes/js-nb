# Coleção de artigos e resumos

## Início

Para ler os artigos utilize as seguintes ferramentas
- [Sci-Hub X - Chrome](https://chrome.google.com/webstore/detail/sci-hub-x-now/gmmnidkpkgiohfdoenhpghbilmeeagjj)
- [Sci-Hub X - Firefox](https://addons.mozilla.org/pt-BR/firefox/addon/sci-hub-x-now/)

## Artigos


### [Fake News Detection Using Naive Bayes Classifier](https://ieeexplore.ieee.org/abstract/document/8100379/) 

>2017

#### Abstract
Abordagem simples para detecção de fake news, foi aplicado em uma coleção de posts do facebook.
Acurácia atingida: 74%


#### Pontos importantes
##### O que viabiliza a utlização do Classificador Naive-Bayes para detectar fake news
>Página 1.

Notícias falsas no geral compartilham algumas características:
- Contém erros gramaticais
- Contem linguagem carregada/emocional
- Tentam manipular a opinião do leitor em alguns tópicos
- Usam vocabulário parecido

>Páginas 2 capítulo IV

O estudo utilizou um dataset do buzzfeed news consistindo de posts do facebook de diferentes fontes que foram checadas manualmente e classificadas em diversas categorias. O estudo ignorou entradas com as classificações "mistura de fato e fake" e "nenhum conteúdo fatídico" 
o

##### Resultados
>Página 3 capítulo VI

As noticias foram detectadas com a seguinte precisão
- Noticias verdadeiras **75.59%**
- Noticias falsas **71.59%**
- Total **75.40%**

A disposição do dataset resultou em uma precisão um pouco menor na detecção de notícias falsas, aproximadamente 95% do  dataset original era composto por notícias verdadeiras

##### Como melhorar a precisão do classificador
>Página 4 capitulo II
- Aumentar dataset de treino
- Usar artigos com maior quantidade de palavras
- Remover stopwords
- Utilizar stemização : **Reduzir formas flexionadas até sua base raiz** 
- Tratar palavras raras separadamente: ignorar tais palavras pode melhorar a precisão

Um modelo mais complexo também poderia ter precisão maior.

##### Conclusão
>Página 4 capitulo VII

O estudo mostra que até um algoritimo simples como o classificador Naive Bayes pode obter bons resultados quando aplicado a problemas como fake news.
