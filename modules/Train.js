import _ from 'lodash';
//a função classifier roda duas vezes: uma pro grupo de noticias verdadeiras e uma pro
//grupo de noticias falsas
const Classifier = (dataSet) => {
    let vocabulario = [];
    const classifiedData = [];
    //para cada noticia
    dataSet.map((data) => {
        //para cada uma das palavras contidas na noticia
        data.corpo.map((palavra) => {
            const current = classifiedData.find(entry => entry.palavra === palavra);
            if (current) {
                current.contagem += 1;
            } else {
            //se for uma palavra nova, o vocabulário cresce
            vocabulario.push(palavra);
                classifiedData.push({
                    'palavra': palavra,
                    'contagem': 1,
                    'probabilidade': 0.0,
                });
            }
        });
    });
    //para cada palavra no classificador
    classifiedData.map((data) => {
        data.probabilidade = data.contagem/ (classifiedData.length || 1);
    });

    return { 'data' : classifiedData, 'vocabulario' : vocabulario };

}
const Train = (dataTrue, dataFalse) => {
    const verdadeiras = Classifier(dataTrue);
    const falsas = Classifier(dataFalse);
    const vocab = _.union(verdadeiras.vocabulario, falsas.vocabulario);
    console.log('vocab:' + vocab.length);
    return ({
        'classV': verdadeiras.data,
        'classF': falsas.data,
        'vocab': vocab.length,
    });
}
export default Train;
