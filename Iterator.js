import NB from './NB.js'
const iterations = 500;
let acc, f1, sens, fPrec = 0.0;
let i, t, instance, total;
for (i = 0; i < iterations; i++) {
    instance = NB();
    t = instance.conMatrix;
    total = t.VP + t.VN + t.FP + t.FN;
    acc += (((t.VP + t.VN) / total) * 100);
    fPrec += ((t.VN / (t.VN + t.FN)) * 100);
    sens += ((t.VN / (t.VN + t.FP))*100);
    f1 += ((2*t.VP / (2*t.VP + t.FP + t.FP))* 100);
}
console.log('---------------------------------------')
console.log('Acurácia em ' + iterations + ' iterações: ' + (acc / iterations).toFixed(2) + '%');
console.log('Precisão fake em ' + iterations + ' iterações: ' + (fPrec / iterations).toFixed(2) + '%');
console.log('Sensibilidade em ' + iterations + ' iterações: ' + (sens / iterations).toFixed(2) + '%');
console.log('F1 Score em ' + iterations + ' iterações: ' + (f1 / iterations).toFixed(2) + '%');
