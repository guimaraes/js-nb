import {readFileSync} from "fs";
import Papa from 'papaparse';
import Tokenize from './Tokenize.js'


//lê os dados csv do arquivo db.csv e retorna a seguinte estrutura
//{corpo: array de palavras, filtradas por stopwords
//tipo: 0,1 . Noticia falsa ou verdadeira}
const CSVData = () => {
    const csv = readFileSync('bd.csv', 'utf8');
    const parsed = Papa.parse(csv);
    const output = parsed.data.map((data) => {
        //se possuir corpo e tipo
        if (data[2]) {
            return {
                'corpo': Tokenize(data[2]),
                'tipo': data[3],
            }
        } else {
            return {
                'corpo': '',
                'tipo': 3,
            }
        }

    });

    return output.filter(entry => entry.tipo != 3);


}
export default CSVData;
