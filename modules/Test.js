import { remove } from "remove-accents";
const ClassifierTest = (classifier, prob, alpha, noticia) => {
    prob = Math.log(prob);
    let corpo = noticia.corpo;
    corpo.map((palavra) => {
        const current = classifier.find(entry => entry.palavra === palavra);
        if (current) {
            prob += Math.log(current.probabilidade + alpha);

        } else {
            prob += Math.log(alpha);
        }
    });
    return prob;
}

const Test = (model, test) => {
    //para cada noticia

    test.map((noticia) => {
        let probV = ClassifierTest(model.classV, model.probV, model.alpha, noticia)
        let probF = ClassifierTest(model.classF, model.probF, model.alpha, noticia)
        let prediction = (probV > probF);
        if (prediction == 1) {
            if (noticia.tipo == 1) {
                model.conMatrix.VP += 1;

            } else {
                model.conMatrix.FP += 1;
            }

        } else {
            if (noticia.tipo == 0) {
                model.conMatrix.VN += 1;

            } else {
                model.conMatrix.FN += 1;
            }
        }
    });
    return model;
}

export default Test;
